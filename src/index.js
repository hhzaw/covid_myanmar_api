const express = require('express');
const app = express();
const scraper = require('table-scraper');
const db = require('./database');
const port = process.env.PORT || 5000;
app.use(function(req, res, next) {

    res.header("Access-Control-Allow-Origin", "*");
  
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  
    res.header("Access-Control-Allow-Methods" , "GET,POST,OPTIONS,DELETE,PUT,PATCH");
  
    next();
  
  });
app.use(express.json());


scrapAllCountries = () => {
    scraper
    .get('https://worldometers.info/coronavirus/')
    .then(async (tableData) => {
        // console.log(tableData[0].length);
        for(country of tableData[0]) {
            // console.log(country['Country,Other']);
                try {
                    //await res.send(country);
                    await saveCountry(country);
                }
                catch(err) {
                    console.log(err);
                }
        }
    })
    .then(() => {
        // console.log('All saved!');
    })
    .catch((err) => {
        console.log(err);
    })
}
app.get('/getAllCountriesData', async(req, res) => {
    try {
        var sql = "select * from countries";
        var params = [];
        db.all(sql, params, (err, rows) => {
            if (err) {
            res.status(400).json({"error":err.message});
            return;
            }
            res.json({
                "message":"success",
                "data":rows
            })
        });
    } catch (error) {
        console.log(error);
    }
})
app.get('/getOneCountryData/:country', async(req, res) => {
    try {
        var sql = "select * from countries where Country = ? ";
        var params = [req.params.country];
        await db.get(sql, params, (err, row) => {
            if (err) {
              res.status(400).json({"error":err.message});
              return;
            }
            res.json({
                "message":"success",
                "data":row
            })
          });
    } catch (error) {
        console.log(error);
    }
})
app.get('/getCountriesList', async(req, res) => {
    try {
        var sql = "SELECT Country, CAST(REPLACE(TotalCases, ',', '') AS INT) as TotalCases  FROM countries ORDER BY TotalCases DESC";
        await db.all(sql, [], (err, rows) => {
            res.send(rows);
        })
    } catch (error) {
        console.log(error);
    }
})
function saveCountry(country) {
    var params = [country['Country,Other']];
    var sql = "SELECT * FROM countries WHERE country = ?";
    db.get(sql, params, function (err, rows) {
        if (!rows){
            var data = {
                Country: country['Country,Other'],
                TotalCases: country['TotalCases'],
                NewCases: country['NewCases'],
                TotalDeaths: country['TotalDeaths'],
                NewDeaths: country['NewDeaths'],
                TotalRecovered: country['TotalRecovered'],
                ActiveCases: country['ActiveCases'],
                Serious: country['Serious,Critical'],
                TotCases_1M: country['Tot Cases/1M pop'],
                TotDeaths_1M: country['Tot Deaths/1M pop'],
            };
            var sql = "INSERT INTO countries (Country, TotalCases, NewCases, TotalDeaths, NewDeaths, TotalRecovered, ActiveCases, Serious, TotCases_1M, TotDeaths_1M) VALUES (?,?,?,?,?,?,?,?,?,?)";
            
            var params =[data.Country, data.TotalCases, data.NewCases, data.TotalDeaths, data.NewDeaths, data.TotalRecovered, data.ActiveCases, data.Serious, data.TotCases_1M, data.TotDeaths_1M];
            db.run(sql, params, function (err, result) {
                if (err){
                    console.log(err);
                }
                // console.log('Save new one');
            });
        }
        else {
            var data = {
                Country: country['Country,Other'],
                TotalCases: country['TotalCases'],
                NewCases: country['NewCases'],
                TotalDeaths: country['TotalDeaths'],
                NewDeaths: country['NewDeaths'],
                TotalRecovered: country['TotalRecovered'],
                ActiveCases: country['ActiveCases'],
                Serious: country['Serious,Critical'],
                TotCases_1M: country['Tot Cases/1M pop'],
                TotDeaths_1M: country['Tot Deaths/1M pop'],
            };
            var sql = "UPDATE countries SET TotalCases=?, NewCases=?, TotalDeaths=?, NewDeaths=?, TotalRecovered=?, ActiveCases=?, Serious=?, TotCases_1M=?, TotDeaths_1M=? WHERE Country = ?"
  
            
            var params =[data.TotalCases, data.NewCases, data.TotalDeaths, data.NewDeaths, data.TotalRecovered, data.ActiveCases, data.Serious, data.TotCases_1M, data.TotDeaths_1M, data.Country ];
            db.run(sql, params, function (err, result) {
                if (err){
                    console.log(err);
                }
                // console.log('Update one')
            });
        }
        //console.log('Successful 1 row');
    });
    
}
setInterval(() => {
    scrapAllCountries()
}, 10000)

app.get('/getNews', (req, res) => {
    var sql = "select * from news";
    var params = [];
    db.all(sql, params, (err, rows) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":rows
        })
      });
})
app.post('/postNews', (req, res) => {
    var errors=[]
    if (!req.body.title){
        errors.push("No title specified");
    }
    if (!req.body.link){
        errors.push("No link specified");
    }
    if (!req.body.info){
        errors.push("No info specified");
    }
    if (errors.length){
        res.status(400).json({"error":errors.join(",")});
        return;
    }
    var data = {
        title: req.body.title,
        link: req.body.link,
        info: req.body.info
    }
    var sql ='INSERT INTO news (title, link, info) VALUES (?,?,?)';
    var params =[data.title, data.link, data.info];
    db.run(sql, params, function (err, result) {
        if (err){
            res.status(400).json({"error": err.message})
            return;
        }
        res.json({
            "message": "success",
            "data": data,
            "id" : this.lastID
        })
    });
})

app.listen(port, () => console.log(`Server is listening at ${port}`));