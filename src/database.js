var sqlite3 = require('sqlite3').verbose()

const DBSOURCE = "db.sqlite"

let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
      // Cannot open database
      console.error(err.message)
      throw err
    }else{
        console.log('Connected to the SQLite database.')
        db.run(`CREATE TABLE news (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            title text, 
            link text UNIQUE, 
            info text, 
            CONSTRAINT link_unique UNIQUE (link)
            )`,
        (err) => {
            if (err) {
                console.log(err);
            }
        });  
        db.run(`CREATE TABLE countries (
            Country text PRIMARY KEY, 
            TotalCases INTEGER, 
            NewCases text, 
            TotalDeaths INTEGER,
            NewDeaths text,
            TotalRecovered INTEGER,
            ActiveCases INTEGER,
            Serious INTEGER,
            TotCases_1M text,
            TotDeaths_1M text
            )`,
        (err) => {
            if (err) {
                console.log(err);
            }
        });  

        
    }
});


module.exports = db